import numpy as np
import initSudoku as init

WIDTH = init.WIDTH
HEIGHT = init.HEIGHT

#create a 2d 9*9 array full of null values -- grid in game
grid = np.empty(shape=[WIDTH,HEIGHT], dtype=object)

#possible values for each cell
#if there is just one value, the cell is solved
allPossible = set((1,2,3,4,5,6,7,8,9))
possible = [[allPossible for v in range(WIDTH)] for v in range(HEIGHT)]


def initializeGrid():
    init.addSampleSudoku(grid)
    init.setZeros(grid)


def checkForPossible():
    for row in range(HEIGHT):
        for col in range(WIDTH):
            if grid[row][col] == 0:

                noRow = possible[row][col] - set(grid[row]) #take away solved numbers from the cell's row
                pos = noRow - set(grid[:, col])  #check all rows, on the same column (that's what the : does)           

                startCubeRow = (row // 3) * 3
                startCubeCol = (col // 3) * 3
                pos = pos - set(grid[startCubeRow:startCubeRow+3, startCubeCol:startCubeCol+3].flatten())

                possible[row][col] = pos


def completedCell():
    blocked = True #sometimes you can't get a single value for any cell - this prevents infinite loops
    for row in range(HEIGHT):
        for col in range(WIDTH):
            if (len(possible[row][col]) == 1):
                grid[row][col] = possible[row][col].pop() #
                blocked = False
    return blocked

def isSolved():
    if not any(0 in grid for grid in grid):
        return True
    else: 
        return False


initializeGrid()
print(grid)

def solve():
    solved = False
    blocked = False
    while((not solved) & (not blocked)):
        checkForPossible()
        blocked = completedCell()
        solved = isSolved()
        print('\n')
        print(grid)

solve()