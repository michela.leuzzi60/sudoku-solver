def permutation(list, start = 0):
    if (start==len(list)):
        print(list)
    else:
        for i in range(start, len(list)):
            list[start], list[i] = list[i], list[start] #swap
            permutation(list, start+1)
            list[start], list[i] = list[i], list[start] #swap back to previous state


permutation(['J', 'O', 'N'])