import numpy as np
import initSudoku as init


WIDTH = init.WIDTH
HEIGHT = init.HEIGHT

grid = np.empty(shape=[WIDTH,HEIGHT], dtype=object)

allPossible = set((1,2,3,4,5,6,7,8,9))
possible = [[allPossible for v in range(WIDTH)] for v in range(HEIGHT)]

def initializeGrid():
    init.addSampleSudoku(grid)
    init.setZeros(grid)

initializeGrid()
