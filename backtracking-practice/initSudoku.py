filename = 'mini.txt'

WIDTH = 3
HEIGHT = 3

#TODO: automate taking numbers from files for samples (V E R Y  HARD, probs)
def addSampleSudoku(grid):
    with open(filename) as f:
        content = f.read().splitlines()
    
    for line in content:
        data = line.split()
        grid[int(data[0]),int(data[1])] = int(data[2])



def setZeros(grid):
    for i in range(WIDTH):
        for j in range(HEIGHT):
            if grid[i][j] == None:
                grid[i][j] = 0


